"""wip

Revision ID: ad440301e47f
Revises: 02ca927a2b55
Create Date: 2020-12-18 10:42:07.689266

"""

# revision identifiers, used by Alembic.
revision = 'ad440301e47f'
down_revision = '02ca927a2b55'

from alembic import op
import sqlalchemy as sa

import warnings

from gertty.dbsupport import sqlite_alter_columns


def upgrade():
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        op.add_column('change', sa.Column('wip', sa.Boolean()))
        op.add_column('change', sa.Column('pending_wip', sa.Boolean()))
        op.add_column('change', sa.Column('pending_wip_message', sa.Text()))

    connection = op.get_bind()
    change = sa.sql.table('change',
                          sa.sql.column('wip', sa.Boolean()),
                          sa.sql.column('pending_wip', sa.Boolean()))
    connection.execute(change.update().values({'wip':False,
                                               'pending_wip':False}))

    sqlite_alter_columns('change', [
        sa.Column('wip', sa.Boolean(), index=True, nullable=False),
        sa.Column('pending_wip', sa.Boolean(), index=True, nullable=False),
        ])


def downgrade():
    pass
