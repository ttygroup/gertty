"""fix_server_table

Revision ID: 02ca927a2b55
Revises: 45d33eccc7a7
Create Date: 2020-12-18 10:41:24.274607

"""

# revision identifiers, used by Alembic.
revision = '02ca927a2b55'
down_revision = '45d33eccc7a7'

from alembic import op
import sqlalchemy as sa


# The original had a reference to own_account.key which is invalid and
# caused later upgrades to fail.  Drop and recreate the table to
# correct; Gertty will fill in the data again.

def upgrade():
    op.drop_table('server')
    op.create_table('server',
    sa.Column('key', sa.Integer(), nullable=False),
    sa.Column('own_account_key', sa.Integer(), sa.ForeignKey('account.key'), index=True),
    sa.PrimaryKeyConstraint('key')
    )


def downgrade():
    pass
